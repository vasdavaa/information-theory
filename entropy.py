import math
import numpy as np
import re
import os.path
import sys


# Beolvasás
def getFileNameFromUser():
    if(len(sys.argv) > 1):
        fileName = sys.argv[1]
    else:
        print('Kérem a fájl nevét:')
        fileName = input()
    if(os.path.isfile(fileName)):
        return fileName
    else:
        print("Nincs ilyen fájl")
        sys.exit()

# Entrópia számítás
# return: Entrópia értéke (konstans érték)
def calcEntropy(array):
    return -np.sum(array * np.log2(array))

# Együttes entrópia számítás
# return: Együttes entrópia értéke (konstans érték)
def calcJointShannonEntropy(array):
    result = 0
    for row in range(len(array)):
        for col in range(len(array[row])):
            if array[row][col] != 0:
                result -= array[row][col] * np.log2(array[row][col])
    return result


# Peremeloszlások számítása
# return: Peremeloszlás értékei (1d Lista)
def calcMargin(arr):
    result = []
    for i in arr:
        result.append(np.sum(i))
    return result


# Feltételes eloszlás számítása
# return: Feltételes eloszlás értéke (konstans érték)
def calcConditionalDistribution(arr, margins):
    entropies = []
    for row in range(len(arr)):
        for col in range(len(arr[row])):
            if arr[row][col] != 0:
                entropies.append(
                    -(arr[row][col] * (math.log2(arr[row][col] / margins[row])))
                )
    return np.sum(entropies)


# Kölcsönös információmennyiség számítása
# return Kölcsönös információmennyiség értéke (konstans érték)
def calcMutualInformation(H_X, H_Y, H_XY):
    return (H_X + H_Y) - H_XY


def run(arr):
    # File megnyitás írásra
    f = open("eredmeny.txt", "w", encoding="utf-8")

    # Számolt eredmények fájlba írása
    f.write("Peremeloszlás entrópiák:\n")
    yMargins = calcMargin(arr)
    xMargins = calcMargin(arr.T)
    H_X = calcEntropy(yMargins)
    H_Y = calcEntropy(xMargins)
    f.write("H(X) = " + str(H_X) + "\n")
    f.write("H(Y) = " + str(H_Y) + "\n")
    f.write("Feltételes entrópiák:\n")
    H_X_I_Y = calcConditionalDistribution(arr, yMargins)
    H_Y_I_X = calcConditionalDistribution(arr.T, xMargins)
    f.write("H(Y|X) = " + str(H_X_I_Y) + "\n")
    f.write("H(X|Y) = " + str(H_Y_I_X) + "\n")
    f.write("Együttes entrópia:\n")
    H_XY = calcJointShannonEntropy(arr)
    f.write("H(X,Y) = " + str(H_XY) + "\n")
    f.write("Kölcsönös információmennyiség:\n")
    f.write("I(X|Y) = " + str(calcMutualInformation(H_X, H_Y, H_XY)))

    # Fájl lezárás
    f.close()
    print("Az eredmények írása az 'eredmeny.txt' fájlba, sikeresen megtörtént!")


if __name__ == '__main__':
    file = open(getFileNameFromUser(), "r")
    fileLines = re.split("\n", file.read())
    matrix = []
    for line in fileLines:
        tempLine = re.sub('[\s+]', '', line)
        matrix.append(list(map(float, re.split(',', tempLine))))
    run(np.array(matrix))
